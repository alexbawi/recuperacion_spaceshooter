﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyManager : MonoBehaviour
{
    public GameObject[] formaciones;
    public float timeLaunchFormation;
    public float timeToBoss = 60;


    private float currentTime = 0;

    void Awake(){
        StartCoroutine(LanzaFormacion());
        StartCoroutine(WaitAndLoadBoss());
    }

    IEnumerator LanzaFormacion(){
        int formacionActual=0;
        while(true){
            formacionActual = Random.Range(0,formaciones.Length); 

            Instantiate(formaciones[formacionActual],new Vector3(10,Random.Range(-5,5)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLaunchFormation);
        }
    }


    private IEnumerator WaitAndLoadBoss()
    {
        yield return new WaitForSeconds(timeToBoss);
        SceneManager.LoadScene("Boss");
    }

}