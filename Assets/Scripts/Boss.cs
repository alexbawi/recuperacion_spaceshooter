﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{

    public int maxHealth = 100;
    public int currentHealth;
    public float delayDisparo = 2;
    [SerializeField] GameObject bullet;
    public float tamanoBala = 5;
    public float minY;
    public float maxY;

    public HealthBar healthBar;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        StartCoroutine(CicloDisparo());
    }

    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            TakeDamage(2);
        }
    }

    void TakeDamage(int damage)
    {
        currentHealth -= damage;

        healthBar.SetHealth(currentHealth);

        if (currentHealth == 0)
        {
            SceneManager.LoadScene("Game");
        }
    }

    private IEnumerator CicloDisparo()
    {
        while (true){
            yield return new WaitForSeconds(delayDisparo);
            GameObject bala = Instantiate(bullet, new Vector3 (transform.position.x, Random.Range(minY, maxY)), Quaternion.Euler(0, 0, 180), null);
            bala.GetComponent<Transform>().localScale = new Vector3(tamanoBala, tamanoBala, tamanoBala);
        }
    }

}