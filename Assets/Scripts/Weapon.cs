﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon: MonoBehaviour
{
    public AudioSource audioSource;
    public abstract float GetCadencia();

    public abstract void Shoot();
}
