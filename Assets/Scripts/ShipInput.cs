﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour
{
    private Vector2 axis;

    public PlayerBehaviour ship;
    public Background bg;

  
    // Update is called once per frame
    void Update () {
        axis.x = Input.GetAxis ("Horizontal");
        axis.y = Input.GetAxis ("Vertical");

        if(Input.GetButton("Fire1")){
            ship.Shoot(0);
        }

        else if (Input.GetButton("Fire2"))
        {
            ship.Shoot(1);
        }

        else if (Input.GetButton("Fire3"))
        {
            ship.Shoot(2);
        }

        ship.ActualizaDatosInput(axis);
        bg.SetVelocity(axis.x);
    }

}
